import { Calculator } from './calculator';

const calculator: Calculator = new Calculator();

const result: number = calculator.integral(1, 5, (x: number) => 2 * x);
console.log('result = ', result);
