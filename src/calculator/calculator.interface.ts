export interface ICalculator {
  add(a: number, b: number): number;
  substract(a: number, b: number): number;
  multiply(a: number, b: number): number;
  divide(a: number, b: number): number;

  integral(start: number, end: number, iteratee: (x: number) => number): number;
}
