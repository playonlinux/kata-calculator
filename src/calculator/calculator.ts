import { ICalculator } from './calculator.interface';
export class Calculator implements ICalculator {
  add(a: number, b: number): number {
    return a + b;
  }
  substract(a: number, b: number): number {
    return a - b;
  }
  multiply(a: number, b: number): number {
    return a * b;
  }
  divide(a: number, b: number): number {
    return b === 0 ? Infinity : a / b;
  }

  integral(start: number, end: number, iteratee: (x: number) => number): any {
    const range: number[] = [];
    for (let i = start; i <= end; i++) {
      range.push(i);
    }
    return range.reduce(
      (carry: number, current: number) => carry + iteratee(current),
      0
    );
  }

  pythagoras(a: number, b: number): number {
    return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
  }
}
