import chai from 'chai';
import { Calculator } from './../../src/calculator';
describe('Calculator test suite', () => {
  const calculator: Calculator = new Calculator();

  describe('add method', () => {
    it('add(1,2) equals 3', () => {
      chai.expect(calculator.add(1, 2)).to.equals(3);
    });
    it('add(2,3) equals 5', () => {
      chai.expect(calculator.add(2, 3)).to.equals(5);
    });
  });
  describe('substract method', () => {
    it('substract(2, 3) equals -1', () => {
      chai.expect(calculator.substract(2, 3)).to.equals(-1);
    });
    it('substract(8, 3) equals 5', () => {
      chai.expect(calculator.substract(8, 3)).to.equals(5);
    });
  });

  describe('multiply method', () => {
    it('multiply(2, 3) equals 6', () => {
      chai.expect(calculator.multiply(2, 3)).to.equals(6);
    });
    it('multiply(8, 3) equals 24', () => {
      chai.expect(calculator.multiply(8, 3)).to.equals(24);
    });
  });

  describe('divide method', () => {
    it('divide(12, 3) equals 4', () => {
      chai.expect(calculator.divide(12, 3)).to.equals(4);
    });
    it('divide(28, 4) equals 7', () => {
      chai.expect(calculator.divide(28, 4)).to.equals(7);
    });
  });

  describe('integral method', () => {
    it('integral(1, 3, 4x + 2) equals 30', () => {
      chai
        .expect(
          calculator.integral(
            1,
            3,
            (x: number) => 4 * x + 2 - (3 * x + 7)
            // calculator.substract(
            //   calculator.add(calculator.multiply(4, x), 2),
            //   calculator.add(calculator.multiply(3, x), 7)
            // )
          )
        )
        .to.equals(-9);
    });
    it('integral(2, 5, 7x - 2) equals 90', () => {
      chai
        .expect(
          calculator.integral(
            2,
            5,
            (x: number) => 7 * x - 2
            // calculator.substract(calculator.multiply(7, x), 2)
          )
        )
        .to.equals(90);
    });
  });

  describe('pythagoras method', () => {
    it('pythagoras(6, 4) equals Math.sqrt(Math.pow(6, 2) + Math.pow(4, 2))', () => {
      chai
        .expect(calculator.pythagoras(6, 4))
        .to.equals(Math.sqrt(Math.pow(6, 2) + Math.pow(4, 2)));
    });
    it('pythagoras(8, 3) equals Math.sqrt(Math.pow(8, 2) + Math.pow(3, 2))', () => {
      chai
        .expect(calculator.pythagoras(8, 3))
        .to.equals(Math.sqrt(Math.pow(8, 2) + Math.pow(3, 2)));
    });
  });
});
